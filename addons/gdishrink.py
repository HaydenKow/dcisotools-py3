#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
    gdishrink, shrinks a gdi dump to a smaller size.
    mrneo240 2021

    gdishrink.py is released under the GNU General Public License
    (version 3), a copy of which (GNU_GPL_v3.txt) is provided in the
    license folder.
"""

from gditools import gdishrink
import os
import sys
sys.path.append('..')
sys.path.append('.')


def main(argv):
    if len(argv) > 1 and os.path.isfile(argv[1]):
        gdishrink(*argv[1:], *argv[2:])
    else:
        print('gdishrink, converts a gdi dump into a valid iso file\n')
        print('Usage: gdishrink.py disc.gdi out_dir')
        print('\mrneo240 2021')


if __name__ == '__main__':
    main(sys.argv)
