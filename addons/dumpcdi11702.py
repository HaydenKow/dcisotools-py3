#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
    dumpcdi11702, GDITools or CDITools?

    Uses gditools to extract a 11702 CDI file.
    Should work for most 11702 CDI file, but wasn't tested much.
    
    It should be possible to extract most CDI by tweaking the offset and 
    wormhole parameters. The proof is left as an exercise to the reader.

    FamilyGuy 2020


    dumpcdi11702.py is released under the GNU General Public License 
    (version 3), a copy of which (GNU_GPL_v3.txt) is provided in the 
    license folder.
"""

import os,sys
sys.path.append('..')

from gditools import ISO9660

def main(filename):
    a = [dict(
        filename=os.path.abspath(filename),
        mode=2336,
        offset=11702*2048 - 1239040,
        wormhole=[0, 11702*2048, 32*2048],
        manualRawOffset=224,
        lba=45000,  # Required to trick ISO9660 class, isn't actually used.
        tnum=0      # Idem
    ),] # List to trick ISO9660 class, need to fix for clean CDI support.
    
    b = ISO9660(a, verbose=True)
    
    for i in b._sorted_records('EX_LOC'):
        if not i['name'] in ['/0.0', '/DUMMY.DAT']:
            b.dump_file_by_record(i, './data')
            #b.dump_file_by_record(i, './'+b.get_pvd()['volume_identifier'])
    
    b.dump_sorttxt()
    b.dump_bootsector(lba=11702)


if __name__ == '__main__':
    if len(sys.argv) == 2:
        main(sys.argv[1])
    else:
        print('dumpcdi11702.py - Based on gditools\n\nError: Bad syntax\n\nUsage: dumpcdi11702.py image.cdi')



